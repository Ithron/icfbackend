package hu.ithron.own.icf.backend.service.component.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import hu.ithron.own.icf.backend.data.entity.RoleEntity;
import hu.ithron.own.icf.backend.data.entity.UserEntity;
import hu.ithron.own.icf.backend.data.repository.UserRepository;

@Component
public class ICFBackendAuthProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder bycriptPasswordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Authentication auth = null;

        String loginUsername = authentication.getName();
        String loginPassword = authentication.getCredentials().toString();
        UserEntity userEntity = getUserEntity(loginUsername);

        if (passwordsMatch(loginPassword, userEntity.getPassword())) {
            List<GrantedAuthority> grantedAuthorities = getUserAuthorities(userEntity.getRoles());

            auth = new UsernamePasswordAuthenticationToken(loginUsername, userEntity.getPassword(), grantedAuthorities);
        } else {
            throw new BadCredentialsException("Authentication failed!");
        }

        return auth;
    }

    private UserEntity getUserEntity(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username);

        if (userEntity == null) {
            throw new UsernameNotFoundException("Username not found: " + username);
        }

        return userEntity;
    }

    private boolean passwordsMatch(String loginPassword, String userPassword) {
        return bycriptPasswordEncoder.matches(loginPassword, userPassword);
    }

    private List<GrantedAuthority> getUserAuthorities(List<RoleEntity> roles) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        for (RoleEntity roleEntity : roles) {
            grantedAuthorities.add(new SimpleGrantedAuthority(roleEntity.getRole()));
        }

        return grantedAuthorities;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
