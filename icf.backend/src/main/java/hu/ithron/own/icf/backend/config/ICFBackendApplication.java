package hu.ithron.own.icf.backend.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = "hu.ithron.own.icf.backend.data.repository")
@ComponentScan(basePackages = {"hu.ithron.own.icf.backend.service.component.impl"})
@EntityScan("hu.ithron.own.icf.backend.data.entity")
@Import(SecurityConfiguration.class)
public class ICFBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ICFBackendApplication.class, args);
    }

}
