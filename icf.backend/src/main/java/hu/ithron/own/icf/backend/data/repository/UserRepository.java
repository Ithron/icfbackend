package hu.ithron.own.icf.backend.data.repository;

import org.springframework.data.repository.CrudRepository;

import hu.ithron.own.icf.backend.data.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    UserEntity findByUsername(String username);
}
