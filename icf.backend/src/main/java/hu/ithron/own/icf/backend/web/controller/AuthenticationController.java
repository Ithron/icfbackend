package hu.ithron.own.icf.backend.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import hu.ithron.own.icf.backend.service.component.impl.ICFBackendAuthProvider;

@Validated
@CrossOrigin
@RestController
public class AuthenticationController {

    @Autowired
    private ICFBackendAuthProvider iFBackendAuthProvider;

    public AuthenticationController() {
    }

    @PostMapping("/authenticate")
    public AuthenticationResponse authenticate(@Valid @RequestBody AuthenticationRequest authenticationRequest) {
        UsernamePasswordAuthenticationToken authToken =
            new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        Authentication authentication = this.bbManagerAuthenticationProvider.authenticate(authToken);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return this.userDao.getAuthenticationResponse(
            authenticationRequest.getUsername(),
            RequestContextHolder.currentRequestAttributes().getSessionId()
        );
    }

    @GetMapping("/logout")
    public StatusResponse logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
        logoutHandler.setInvalidateHttpSession(true);
        logoutHandler.setClearAuthentication(true);

        logoutHandler.logout(request, response, auth);
        
        return new StatusResponse(Constants.StatusResponses.OK);
    }
}
