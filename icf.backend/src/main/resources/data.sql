INSERT INTO roles(id, role) VALUES
(1, 'Adminisztrátor'),
(2, 'Tartalomszerkesztő'),
(3, 'Bejelentkezett felhasználó');

INSERT INTO users(id, username, password) VALUES
(1, 'Admin', 'AdminPass'),
(2, 'User 1', 'User1Pass'),
(3, 'User 2', 'User2Pass'),
(4, 'User 3', 'User3Pass');

INSERT INTO user_roles(id, user_id, role_id) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 2, 3),
(4, 3, 2),
(5, 4, 3);