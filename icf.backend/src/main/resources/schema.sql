DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS roles;

CREATE TABLE roles(
    id TINYINT PRIMARY KEY AUTO_INCREMENT,
    role VARCHAR(30) NOT NULL
);

CREATE TABLE users(
    id TINYINT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(60) NOT NULL,
    enabled BOOLEAN DEFAULT TRUE,
    last_login_date TIMESTAMP DEFAULT NULL
);

create table user_roles(
    id TINYINT PRIMARY KEY AUTO_INCREMENT,
    user_id TINYINT NOT NULL,
    role_id TINYINT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (role_id) REFERENCES roles(id)
);